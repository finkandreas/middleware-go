# build server software in a temporary container
FROM ubuntu:22.04 as builder

ARG FILEBEAT_VERSION=8.5.2

# add repo with latest golang and install golang
RUN apt update && \
    apt install -y software-properties-common && \
    add-apt-repository ppa:longsleep/golang-backports && \
    apt update && \
    apt install -y golang curl git

# download filebeat
RUN cd /tmp \
  && curl -L https://artifacts.elastic.co/downloads/beats/filebeat/filebeat-${FILEBEAT_VERSION}-linux-x86_64.tar.gz > filebeat.tar.gz \
  && tar -xf filebeat.tar.gz \
  && cp filebeat-${FILEBEAT_VERSION}-linux-x86_64/filebeat /tmp/filebeat

# build sources
COPY . /sourcecode
WORKDIR /sourcecode
RUN go build ./apps/server
RUN go build ./apps/background_tasks

# Final container - copy from builder container
FROM ubuntu:22.04

RUN apt update && apt upgrade -y && env DEBIAN_FRONTEND=noninteractive apt install -y  tzdata ca-certificates skopeo git && rm -Rf /var/lib/apt/lists/* && echo test

# go server component
COPY --from=builder /sourcecode/server /usr/local/bin
COPY --from=builder /sourcecode/background_tasks /usr/local/bin
COPY apps/server/templates /opt/ci_go/templates
COPY apps/server/static    /opt/ci_go/static

# filebeat
RUN mkdir -p /opt/filebeat
COPY --from=builder /tmp/filebeat /opt/filebeat/filebeat
