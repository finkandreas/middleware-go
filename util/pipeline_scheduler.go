package util;

import (
    "crypto/sha256"
    "encoding/hex"
    "fmt"
    "io/ioutil"
    "net/url"
    "os"
    "strings"
    "sync"
    "time"

    "github.com/go-co-op/gocron"
    "github.com/xanzy/go-gitlab"
    "github.com/rs/zerolog"
    gossh "golang.org/x/crypto/ssh"
    "github.com/go-git/go-git/v5"
    gitconfig "github.com/go-git/go-git/v5/config"
    "github.com/go-git/go-git/v5/plumbing"
    "github.com/go-git/go-git/v5/plumbing/transport"
    gitssh "github.com/go-git/go-git/v5/plumbing/transport/ssh"

    "cscs.ch/cicd-ext-mw/logging"
)

var locked_repo_dirs sync.Map

type CronScheduler struct {
    glserver *gitlab.Client
    db DB
    config Config
}

func NewCronScheduler(glserver *gitlab.Client, db DB, config Config) CronScheduler {
    return CronScheduler{glserver, db, config}
}

func (c CronScheduler) ScheduleJobs() {
    scheduler := gocron.NewScheduler(time.Local)
    scheduler.StartAsync()
    jobsBySID := make(map[int64]schedulerData)
    // poll for cron schedules - This is not optimal, and we should only reread the config when it was changed, but we would need an IPC mechanism first (e.g. Redis)
    for {
        schedules_db := c.db.GetAllPipelineSchedules()
        for _, sdb := range schedules_db {
            // sdb is the schedule as stored in database - i.e. the truth
            if jobDataRunning, exists := jobsBySID[sdb.ScheduleID]; exists {
                // this cron schedule is already running, we need to check if the schedule has changed and reschedule appropriately
                if jobDataRunning.dbData.CronSchedule != sdb.CronSchedule {
                    // the schedule has changed - cancel old job - schedule new job
                    logging.Debugf("Cron scheduling times for schedule_id=%v has changed. Old schedule=%v, new schedule=%v - rescheduling", sdb.ScheduleID, jobDataRunning.dbData.CronSchedule, sdb.CronSchedule)
                    scheduler.RemoveByReference(jobDataRunning.job)
                    c.schedule_job(sdb, scheduler, jobsBySID)
                } else if jobDataRunning.dbData.Ref != sdb.Ref {
                    // Reference has changed, keep track of it
                    logging.Debugf("Cron trigger ref has changed for schedule_id=%v. Old ref=%v, new ref=%v", sdb.ScheduleID, jobDataRunning.dbData.Ref, sdb.Ref)
                    entry := jobsBySID[sdb.ScheduleID]
                    entry.dbData = sdb
                }
            } else {
                // this cron schedule has not yet been seen/started
                c.schedule_job(sdb, scheduler, jobsBySID)
            }
        }

        // reread schedules from DB every 10 minutes
        time.Sleep(10*time.Minute)
    }
}

func (c *CronScheduler) schedule_job(schedule_db ScheduleDataNoVars, scheduler *gocron.Scheduler, jobsBySID map[int64]schedulerData) {
    job, err := scheduler.Cron(schedule_db.CronSchedule).Do(func(){
        c.trigger_pipeline(schedule_db.ScheduleID)
    })
    if err != nil {
        logging.Error(err, "Failed scheduling job")
    } else {
        jobsBySID[schedule_db.ScheduleID] = schedulerData{schedule_db, job}
    }
}

func (c *CronScheduler) trigger_pipeline(schedule_id int64) {
    logger := logging.Get()
    schedule_data, pipeline_id := c.db.GetPipelineSchedule(schedule_id)

    // clone or open repository - we assume here that no other task is currently working on the repository
    // this assumption is valid as long as this is cloning to a different directory as the webhook_ci handler
    repo_id := c.db.GetRepositoryId(pipeline_id)
    target_path := fmt.Sprintf("/home/tmp/scheduled/%v", repo_id)
    LockRepoDir(target_path, logger)
    defer UnlockRepoDir(target_path, logger)
    repo, err := GitCloneOrOpen(&c.db, &c.config, target_path, repo_id, true, logger, nil)
    if err != nil {
        logger.Error().Err(err).Msgf("Failed cloning repository error=%v", err)
        return
    }
    if err := GitFetch(&c.db, &c.config, repo, "", []gitconfig.RefSpec{gitconfig.RefSpec("+refs/heads/*:refs/heads/*")}, repo_id, logger, nil); err != nil {
        logger.Error().Err(err).Msgf("Failed fetching updates from remote. Error=%v", err)
        return
    }
    hash, err := repo.ResolveRevision(plumbing.Revision(schedule_data.Ref))
    if err != nil {
        logger.Error().Err(err).Msgf("Failed getting SHA for reference %v", schedule_data.Ref)
        return
    }
    if err := EnsureMirrorRepo(&c.db, &c.config, c.glserver, logger, repo_id, pipeline_id); err != nil {
        NotifyError(fmt.Append(nil, "error: ", err, ".<br>"), hash.String(), db.GetPipelineName(pipeline_id), "Error creating mirror repository", repo_id, &c.config, &c.db, logger)
        logger.Error().Err(err).Msgf("Failed creating mirror repository error=%v", err)
        return
    }
    refspecs := []gitconfig.RefSpec{gitconfig.RefSpec(fmt.Sprintf("+refs/heads/%v:refs/heads/%v", schedule_data.Ref, schedule_data.Ref))}
    if err := GitPush(&c.db, &c.config, repo, refspecs, repo_id, pipeline_id, logger, nil); err != nil {
        NotifyError(fmt.Append(nil, "error: ", err, ".<br>"), hash.String(), db.GetPipelineName(pipeline_id), "Error pushing to mirror repository", repo_id, &c.config, &c.db, logger)
        logger.Error().Err(err).Msgf("Failed pushing branches to mirror repository. error=%v", err)
        return
    }
    if err := TriggerPipeline(c.glserver, &c.config, &c.db, pipeline_id, schedule_data.Ref, schedule_data.Variables); err != nil {
        NotifyError(fmt.Append(nil, "error: ", err, ".<br>"), hash.String(), db.GetPipelineName(pipeline_id), "Error triggering pipeline", repo_id, &c.config, &c.db, logger)
        logger.Error().Err(err).Msgf("Failed triggering pipeline for schedule_id=%v", schedule_id)
    }
}


func GitCloneOrOpen(db *DB, config *Config, target_path string, repo_id int64, bare_clone bool, logger *zerolog.Logger, progress *os.File) (*git.Repository, error) {
    if progress == nil {
        progress = os.Stdout
    }
    if repo, err := git.PlainOpen(target_path); err != nil {
        // could not open repo, we need to clone it
        clone_options := git.CloneOptions{
            URL: GitUrlOrig(db, logger, repo_id),
            Auth: GitAuthOrig(config, logger, repo_id),
            Progress: progress,
        }
        logger.Info().Msgf("Cloning to %v with options=%+v", target_path, clone_options)
        return git.PlainClone(target_path, bare_clone, &clone_options)
    } else {
        logger.Info().Msgf("path=%v exists already, using existing git repository", target_path)
        return repo, nil
    }
}

func GitFetch(db *DB, config *Config, repo *git.Repository, fromRepo string, refspecs []gitconfig.RefSpec, repo_id int64, logger *zerolog.Logger, progress *os.File) error {
    if progress == nil {
        progress = os.Stdout
    }
    fetchOptions := git.FetchOptions{
        Force: true,
        RemoteURL: GitUrlOrig(db, logger, repo_id),
        Auth: GitAuthOrig(config, logger, repo_id),
        RefSpecs: refspecs,
        Progress: progress,
    }
    if fromRepo != "" {
        fetchOptions.RemoteURL = fromRepo
    }
    logger.Debug().Msgf("Fetching fetchOptions=%#v", fetchOptions)
    err := repo.Fetch(&fetchOptions)
    if err != nil && err != git.NoErrAlreadyUpToDate {
        return err
    }
    return nil
}

func GitPush(db *DB, config *Config, repo *git.Repository, refspecs []gitconfig.RefSpec, repo_id, pipeline_id int64, logger *zerolog.Logger, progress *os.File) error {
    if progress == nil {
        progress = os.Stdout
    }
    o := &git.PushOptions{
        Force: true,
//            Prune: true, // do not enable Prune, seems to fail pre-receive hook, without further information
        Progress: progress,
        RefSpecs: refspecs,
        Options: map[string]string{"ci.skip":"1"},
        RemoteURL: GitUrlMirror(config, repo_id, pipeline_id),
        Auth: GitAuthMirror(config, pipeline_id, logger),
    }
    logger.Debug().Msgf("Using push options %#v", o)
    if err := repo.Push(o); err != nil {
        if err == git.NoErrAlreadyUpToDate {
            logger.Info().Msgf("Mirror repository is already up to date. Nothing to push")
        } else {
            return err
        }
    }
    return nil
}

func GitUrlOrig(db *DB, logger *zerolog.Logger, repo_id int64) string {
    if db.IsPrivateRepo(repo_id) {
        // transform https://example.com/namespace/project --> example.com:namespace/project
        if u, err := url.Parse(db.GetRepoUrl(repo_id)); err != nil {
            logger.Error().Err(err).Msgf("Failed parsing repository url")
            return ""
        } else {
            return fmt.Sprintf("%v:%v", u.Hostname(), u.Path[1:])
        }
    } else {
        return db.GetRepoUrl(repo_id)
    }
}

func GitAuthOrig(config *Config, logger *zerolog.Logger, repo_id int64) transport.AuthMethod {
    if db.IsPrivateRepo(repo_id) {
        privkey_path := config.GetSSHKeyPath(repo_id, false, false)
        if publicKeys, err := gitssh.NewPublicKeysFromFile("git", privkey_path, "") ; err != nil {
            logger.Error().Err(err).Msgf("Error reading ssh keyfile")
            return nil
        } else {
            publicKeys.HostKeyCallback = gossh.InsecureIgnoreHostKey()
            return publicKeys
        }
    }
    return nil
}

func GitUrlMirror(config *Config, repo_id, pipeline_id int64) string {
    return fmt.Sprintf("%v:%v/%v/%v", strings.Replace(config.Gitlab.Url, "https://", "", 1), config.Gitlab.MirrorsPath, repo_id, pipeline_id)
}

func GitAuthMirror(config *Config, pipeline_id int64, logger *zerolog.Logger) transport.AuthMethod {
    privkey_path := config.GetSSHKeyPath(pipeline_id, true, false)
    if publicKeys, err := gitssh.NewPublicKeysFromFile("git", privkey_path, "") ; err != nil {
        logger.Error().Err(err).Msgf("Error reading ssh keyfile")
        return nil
    } else {
        publicKeys.HostKeyCallback = gossh.InsecureIgnoreHostKey()
        return publicKeys
    }
}

func EnsureMirrorRepo(db *DB, config *Config, glserver *gitlab.Client, logger *zerolog.Logger, repo_id, pipeline_id int64) error {
    mirror_repo_name := fmt.Sprintf("%v-%v", db.GetRepositoryName(repo_id), db.GetPipelineName(pipeline_id))
    if mirror_project, _, err := glserver.Projects.GetProject(fmt.Sprintf("%v/%v/%v", config.Gitlab.MirrorsPath, repo_id, pipeline_id), nil) ; err != nil {
        logger.Info().Err(err).Msgf("No mirror project found for %v. Creating a new mirror project", mirror_repo_name)
        var visibility = gitlab.PrivateVisibility
        if !db.IsPrivateRepo(repo_id) {
            visibility = gitlab.PublicVisibility
        }
        var group *gitlab.Group
        var err error
        if group, _, err = glserver.Groups.GetGroup(fmt.Sprintf("%v/%v", config.Gitlab.MirrorsPath, repo_id), &gitlab.GetGroupOptions{}); err != nil {
            logger.Info().Err(err).Msgf("Could not find group for mirror repositories. Creating a new one")
            if mirrorGroup, _, err := glserver.Groups.GetGroup(fmt.Sprintf("%v", config.Gitlab.MirrorsPath), &gitlab.GetGroupOptions{}); err != nil {
                logger.Error().Err(err).Msg("Could not get top-level mirror groupd from gitlab. Cannot create  mirror repository")
                return err
            } else {
                group, _, err = glserver.Groups.CreateGroup(&gitlab.CreateGroupOptions{
                    Name: gitlab.String(fmt.Sprintf("%v-%v", db.GetRepositoryName(repo_id), repo_id)),
                    Path: gitlab.String(fmt.Sprintf("%v", repo_id)),
                    ParentID: &mirrorGroup.ID,
                    EmailsDisabled: gitlab.Bool(true),
                    Visibility: &visibility,
                })
                if err != nil {
                    return err
                }
            }
        }
        new_project, _, err := glserver.Projects.CreateProject(&gitlab.CreateProjectOptions{
            Name: &mirror_repo_name,
            Path: gitlab.String(fmt.Sprintf("%v", pipeline_id)),
            Description: gitlab.String(fmt.Sprintf("Mirror of %v", db.GetRepoUrl(repo_id))),
            NamespaceID: &group.ID,
            PublicBuilds: gitlab.Bool(true), // this does not allow public access to the build logs for private projects though (True by default anyway, just here for reference)
            InitializeWithReadme: gitlab.Bool(true), // initialize with a readme, which creates a default branch (and a branch protection) such that we can delete the branch protection directly
            Visibility: &visibility,
            EmailsDisabled: gitlab.Bool(true),
            CIConfigPath: gitlab.String(db.GetCIEntrypoint(pipeline_id)),
        })
        if err != nil {
            return err
        }
        db.UpdateMirrorUrl(pipeline_id, new_project.WebURL)
        _, _, err = glserver.Projects.AddProjectHook(new_project.ID, &gitlab.AddProjectHookOptions{
            URL: gitlab.String(fmt.Sprintf("%v/%v/webhook_gitlab_pipeline?repository_id=%v&pipeline_id=%v", config.BaseURL, config.URLPrefix, repo_id, pipeline_id)),
            PipelineEvents: gitlab.Bool(true),
            JobEvents: gitlab.Bool(true),
            PushEvents: gitlab.Bool(false),
            Token: gitlab.String(db.GetMirrorWebhookSecret(pipeline_id)),
        })
        if err != nil {
            logger.Error().Err(err).Msg("Error setting up webhooks in new project mirror")
        }
        _, _, err = glserver.ProjectVariables.CreateVariable(new_project.ID, &gitlab.CreateProjectVariableOptions{
            Key: gitlab.String("CSCS_REGISTRY_PATH"),
            Value: gitlab.String(fmt.Sprintf("%v/%v", config.RegistryPath, repo_id)),
        })
        if err != nil {
            logger.Error().Err(err).Msg("Error creating variable in new project mirror")
        }

        // create SSH keyfiles
        if ssh_pubkey, err := CreateSshKeys(config.GetSSHKeyPath(pipeline_id, true, false)) ; err != nil {
            logger.Error().Err(err).Msg("Failed creating SSH keys")
        } else {
            glserver.DeployKeys.AddDeployKey(new_project.ID, &gitlab.AddDeployKeyOptions{
                Key: gitlab.String(string(ssh_pubkey)),
                Title: gitlab.String("CSCS-CI"),
                CanPush: gitlab.Bool(true),
            })
        }

        // delete branch protection
        if protected_branches, _, err := glserver.ProtectedBranches.ListProtectedBranches(new_project.ID, &gitlab.ListProtectedBranchesOptions{}); err != nil {
            logger.Error().Err(err).Msg("Could not get protected branches from new project mirror")
        } else {
            logger.Debug().Msgf("protected_branches=%#v", protected_branches)
            for _, branch := range protected_branches {
                logger.Debug().Msgf("Unprotecting branch %v in mirror repository", branch.Name)
                if _, err := glserver.ProtectedBranches.UnprotectRepositoryBranches(new_project.ID, branch.Name); err != nil {
                    logger.Error().Err(err).Msgf("Failure unprotecting branch=%v", branch.Name)
                }
            }
        }
        // gitlab needs some time to unprotect the branches - sleep a bit
        time.Sleep(3*time.Second)

        logger.Debug().Msgf("successfully created mirror repository %v", new_project.Name)
    } else {
        logger.Info().Msgf("Mirror repo %v exists already.", mirror_project.Name)
    }
    return nil
}


func TriggerPipeline(glserver *gitlab.Client, config *Config, db *DB, pipeline_id int64, ref string, variables map[string]string) error {
    repo_id := db.GetRepositoryId(pipeline_id)
    all_vars := make(map[string]string)
    all_vars = merge_maps(all_vars, repo_vars(repo_id))
    all_vars = merge_maps(all_vars, pipeline_vars(pipeline_id))
    all_vars = merge_maps(all_vars, variables)
    all_vars = merge_maps(all_vars, ci_impl_vars(config)) // implementation details will always overwrite, because the user should not use CSCS_CI_* as key
    _, _, err := glserver.Pipelines.CreatePipeline(fmt.Sprintf("%v/%v/%v", config.Gitlab.MirrorsPath, repo_id, pipeline_id), &gitlab.CreatePipelineOptions{
        Ref: gitlab.String(ref),
        Variables: convert_as_gitlab_vars(all_vars),
    })
    return err
}


func NotifyError(err_msg []byte, sha string, notification_name, desc string, repo_id int64, config *Config, db *DB, logger *zerolog.Logger) {
    s256 := sha256.Sum256(err_msg)
    html_file := fmt.Sprintf("%v.html", hex.EncodeToString(s256[:]))
    if write_err := ioutil.WriteFile(fmt.Sprintf("%v/%v", config.ErrorsDir, html_file), err_msg, 0644); write_err != nil {
        logger.Error().Err(write_err).Msgf("Failed writing error html-file=%v", html_file)
    }
    target_url := fmt.Sprintf("%v/%v/pipeline/results/error/%v", config.BaseURL, config.URLPrefix, html_file)
    if resp, err := NotifyRepo(GetNotificationUrl(db.GetRepoUrl(repo_id), sha), GlStatusFailed, target_url, notification_name, db.GetNotificationToken(repo_id), desc); err != nil {
        logger.Error().Err(err).Msgf("Failed notifying repository. err=%v", err)
    } else if err := CheckResponse(resp); err != nil {
        // most probably an incorrect notification token
        logger.Error().Err(err).Msgf("Failed notifiying original repository %v", db.GetRepositoryName(repo_id))
    }
}

func LockRepoDir(repo_path string, logger *zerolog.Logger) {
    for {
        if _, loaded := locked_repo_dirs.LoadOrStore(repo_path, true); loaded {
            logger.Info().Msgf("Path=%v is currently locked. Waiting 1s", repo_path)
            time.Sleep(1*time.Second)
        } else {
            logger.Info().Msgf("Path=%v has been successfully locked", repo_path)
            return
        }
    }
}
func UnlockRepoDir(repo_path string, logger *zerolog.Logger) {
    locked_repo_dirs.Delete(repo_path)
    logger.Info().Msgf("Path=%v has been successfully unlocked", repo_path)
}

func merge_maps(a, b map[string]string) map[string]string {
    for k,v := range b {
        a[k] = v
    }
    return a
}


func repo_vars(repo_id int64) map[string]string {
    return nil
}
func pipeline_vars(repo_id int64) map[string]string {
    return nil
}
func ci_impl_vars(config *Config) map[string]string {
    return map[string]string {
        "CSCS_CI_MW_URL": fmt.Sprintf("%v/%v", config.BaseURL, config.URLPrefix),
    }
}

func convert_as_gitlab_vars(vars map[string]string) *[]*gitlab.PipelineVariableOptions {
    var ret []*gitlab.PipelineVariableOptions
    for k,v := range vars {
        ret = append(ret, &gitlab.PipelineVariableOptions{Key: gitlab.String(k), Value: gitlab.String(v)})
    }
    return &ret
}

type schedulerData struct {
    dbData ScheduleDataNoVars
    job *gocron.Job
}
