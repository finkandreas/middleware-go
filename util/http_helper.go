package util

import (
    "encoding/json"
    "fmt"
    "io/ioutil"
    "net/http"
    "time"

    "github.com/hashicorp/go-retryablehttp"
    cleanhttp "github.com/hashicorp/go-cleanhttp"

    "cscs.ch/cicd-ext-mw/logging"
)

// use this if you do not need any additional headers for your request
var NoAdditionalHeaders = map[string]string{}

var retryClient = &retryablehttp.Client{
    HTTPClient:   cleanhttp.DefaultPooledClient(),
    Logger:       nil,
    RetryWaitMin: 1*time.Second,
    RetryWaitMax: 300*time.Second,
    RetryMax:     30,
    CheckRetry:   retryablehttp.DefaultRetryPolicy,
    Backoff:      retryablehttp.DefaultBackoff,
}

// helper struct which has the Body of the response automatically added under ResponseDa
type ResponseHelper struct {
    *http.Response
    ResponseData []byte
}


// helper to create an error if the statuscode >= 400, i.e. not successful
func CheckResponse(resp *ResponseHelper) error {
    if resp == nil {
        return fmt.Errorf("CheckResponse received a nil-response")
    }
    if resp.StatusCode >= 400 {
        return fmt.Errorf("Request for %v failed with statuscode=%v msg=%v", resp.Request.URL.RequestURI(), resp.StatusCode, string(resp.ResponseData))
    }
    return nil
}

func DoJsonRequest(method string, url string, headers map[string]string, data any) (*ResponseHelper, error) {
    var jsondata []byte
    if data != nil {
        var err error
        jsondata, err = json.Marshal(data)
        if err != nil {
            return nil, err
        }
    }

    req, err := retryablehttp.NewRequest(method, url, jsondata)
    if err != nil {
        return nil, err
    }

    if headers != nil {
        for k,v := range headers {
            req.Header.Add(k,v)
        }
    }
    req.Header.Add("Content-Type", "application/json")
    logging.Debugf("DoJsonRequest method=%v url=%v headers=%v, jsondata=%v", method, url, headers, string(jsondata))
    resp, err := retryClient.Do(req)
    if err != nil {
        return nil, err
    }

    var responseData []byte
    if resp.Body != nil {
        responseData, err = ioutil.ReadAll(resp.Body)
        if err != nil {
            return nil, err
        }
    }
    logging.Debugf("DoJsonRequest resp.StatusCode=%v, responseData=%v", resp.StatusCode, string(responseData))
    return &ResponseHelper{resp, responseData}, nil
}

