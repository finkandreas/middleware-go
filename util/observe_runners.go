package util

import (
    "encoding/json"
    "os"
    "time"

    "cscs.ch/cicd-ext-mw/logging"
    "github.com/xanzy/go-gitlab"
)


type RunnersObserver struct {
    logfile *os.File
    runner_ids []int
    glserver *gitlab.Client
}

type RunnerStatus struct {
    Timestamp int64 `json:"timestamp"`
    LastContact int64 `json:"last_contact"`
    Online bool `json:"online"`
    Name string `json:"name"`
    Active bool `json:"active"`
}

func NewRunnersObserver(glserver *gitlab.Client, logpath string, runner_ids []int) RunnersObserver {
    logger := logging.Get()
    file, err := os.OpenFile(logpath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
    if err != nil {
        logger.Error().Err(err).Msgf("Error opening file %v", logpath)
        panic(err)
    }
    return RunnersObserver{file, runner_ids, glserver}
}


func (observer RunnersObserver) Observe() {
    logger := logging.Get()
    for {
        for _, id := range observer.runner_ids {
            details, _, err := glserver.Runners.GetRunnerDetails(id)
            if err != nil {
                logger.Error().Err(err).Msgf("Could not fetch runner details with id=%v. err=%v", id, err)
                continue
            }
            status := RunnerStatus{time.Now().Unix(), details.ContactedAt.Unix(), details.Online, details.TagList[0], details.Active}
            json.NewEncoder(observer.logfile).Encode(status)
        }
        time.Sleep(60*time.Second)
    }
}
