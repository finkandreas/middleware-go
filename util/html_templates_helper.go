package util

import (
    "fmt"
    "html/template"
    "io"
    "os"
    "path"
    "path/filepath"
    "strings"
    "sync"
    "time"

    "github.com/fsnotify/fsnotify"

    "cscs.ch/cicd-ext-mw/logging"
)


var onceTemplate sync.Once
var templates *template.Template
var templates_config TemplatesConfig
var parsed_templates map[string]*template.Template

var template_funcs = map[string]any {
    "lower": func(in string) string { return strings.ToLower(in) },
}

func ParseTemplateDir(dir string) (*template.Template, error) {
    var paths []string
    logging.Debugf("Parsing template files in directory=%v", dir)
    err := filepath.Walk(dir, func(thispath string, info os.FileInfo, err error) error {
        if err != nil {
            return err
        }
        if !info.IsDir() && path.Base(thispath)[0] != '.' {
            paths = append(paths, thispath)
        }
        return nil
    })
    if err != nil {
        return nil, err
    }
    return template.New("test").Funcs(template_funcs).ParseFiles(paths...)
}

func SetTemplatesConfig(config TemplatesConfig) {
    templates_config = config
    for idx, path := range templates_config.AlwaysInclude {
        templates_config.AlwaysInclude[idx] = fmt.Sprintf("%v/%v", templates_config.Dir, path)
    }
    parsed_templates = make(map[string]*template.Template)
}

func TestAllTemplates() *template.Template {
    var err error
    onceTemplate.Do(func() {
        templates, err = ParseTemplateDir(templates_config.Dir)
        if err != nil {
            logging.Error(err, "Error parsing templates directory")
            panic(err)
        }
    })

    return templates
}

func GetTemplate(template_basename string) (*template.Template, error) {
    var err error
    if parsed_templates[template_basename] == nil {
        files := append(templates_config.AlwaysInclude, path.Join(templates_config.Dir, template_basename))
        parsed_templates[template_basename], err = template.New(template_basename).Funcs(template_funcs).ParseFiles(files...)
    }
    return parsed_templates[template_basename], err
}
func ExecuteTemplate(w io.Writer, template_basename string, data any) error {
    tmpl, err := GetTemplate(template_basename)
    if err != nil {
        return err
    }
    return tmpl.ExecuteTemplate(w, template_basename, data)
}

func ListenTemplateChanges(watcher *fsnotify.Watcher) {
    // Start listening for events.
    go func() {
        for {
            select {
            case event, ok := <-watcher.Events:
                if !ok {
                    logging.Warn("Watcher event is not `ok`. Returning watching fsnotify events")
                    return
                }
                if event.Has(fsnotify.Write) && path.Base(event.Name)[0] != '.' {
                    logging.Infof("modified file: %v. Rereading the templates", event.Name)
                    var err error
                    // give some time for the file to be fully written, only then reread the templates
                    time.Sleep(1*time.Second)
                    files := append(templates_config.AlwaysInclude, event.Name)
                    parsed_templates[path.Base(event.Name)], err = template.New(path.Base(event.Name)).Funcs(template_funcs).ParseFiles(files...)
                    if err != nil {
                        logging.Error(err, "Error parsing templates directory")
                    }
                }
            case err, ok := <-watcher.Errors:
                if !ok {
                    return
                }
                logging.Error(err, "watcher error")
            }
        }
    }()
}
