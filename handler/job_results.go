package handler

import (
    "bytes"
    "fmt"
    "html/template"
    "net/http"
    "strconv"


    "github.com/xanzy/go-gitlab"
    "github.com/gorilla/mux"

    "cscs.ch/cicd-ext-mw/thirdparty/ansihtml"
    "cscs.ch/cicd-ext-mw/logging"
    "cscs.ch/cicd-ext-mw/util"
)

func GetJobResultsHandler(glserver *gitlab.Client, config util.Config) func(w http.ResponseWriter, r *http.Request) {
    return wrap(jobResultsHandler{glserver, config})
}

type jobResultsHandler struct {
    Glserver *gitlab.Client
    Config util.Config
}

func (h jobResultsHandler) Get(w http.ResponseWriter, r* http.Request) {
    w.Header().Add("Content-Type", "text/html")
    logger := logging.GetReqLogger(r)
    vars := mux.Vars(r)
    pipeline_id := vars["pipeline_id"]
    gitlab_project_id := vars["gitlab_project_id"]
    job_id, err := strconv.Atoi(vars["job_id"])
    pie(logger, err, "Invalid URL", http.StatusBadRequest)

    job, resp, err := h.Glserver.Jobs.GetJob(gitlab_project_id, job_id)
    pie(logger, err, "Failed fetching job", resp.StatusCode)

    trace_reader, resp, err := h.Glserver.Jobs.GetTraceFile(gitlab_project_id, job_id)
    pie(logger, err, "Failed getting job trace", resp.StatusCode)

    html_trace := new(bytes.Buffer)
    ansihtml.ConvertToHTML(trace_reader, html_trace)

    tmpl_data := jobResultsTmplData{
        Title: fmt.Sprintf("Log for job %v", job.Name),
        JobLog: template.HTML(html_trace.String()),
        PipelineUrl: fmt.Sprintf("/%v/pipeline/results/%v/%v/%v", h.Config.URLPrefix, pipeline_id, gitlab_project_id, job.Pipeline.ID),
        Favicon: status_favicon(job.Status),
        UrlPrefix: h.Config.URLPrefix,
    }

    err = util.ExecuteTemplate(w, "job_result.html", &tmpl_data)
    pie(logger, err, "Error rendering template job_result.html", http.StatusInternalServerError)
}


type jobResultsTmplData struct {
    Title string
    JobLog template.HTML
    PipelineUrl string
    Favicon string
    UrlPrefix string
}
