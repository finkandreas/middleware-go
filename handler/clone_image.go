package handler

import (
    "fmt"
    "math/rand"
    "net/http"
    "os/exec"
    "strings"

    "gopkg.in/yaml.v3"
    "github.com/rs/xid"

    "cscs.ch/cicd-ext-mw/logging"
    "cscs.ch/cicd-ext-mw/util"
)


func GetCloneImageHandler(db util.DB, config util.Config) func(w http.ResponseWriter, r *http.Request) {
    return wrap(cloneImageHandler{DB: db, Config: config})
}

type cloneImageHandler struct {
    DB util.DB
    Config util.Config
}

type registryCreds struct {
    Username string `yaml:"username"`
    Password string `yaml:"password"`
}
type cloneImagePostBody struct {
    Username  int64 `yaml:"username"`
    Password  string `yaml:"password"`
    FromImage string `yaml:"from_image"`
    ToImage   string `yaml:"to_image"`
    RegistryCreds registryCreds `yaml:"registry_credentials"`
}


func (h cloneImageHandler) Post(w http.ResponseWriter, r *http.Request) {
    logger := logging.GetReqLogger(r)

    var clone_descr cloneImagePostBody
    err := yaml.NewDecoder(r.Body).Decode(&clone_descr)
    pie(logger, err, "The provided data in body could not be parsed", http.StatusBadRequest)

    repository_id := clone_descr.Username
    job_id := rand.Int()

    // check if we are authorized to accept this request
    if h.DB.GetWebhookSecret(clone_descr.Username) != clone_descr.Password {
        http.Error(w, "You are not authorized", http.StatusUnauthorized)
        return
    }

    registry_path := h.Config.GetRegistryPathForRepo(clone_descr.Username)
    if strings.HasPrefix(clone_descr.FromImage, "$CSCS_REGISTRY_PATH") {
        clone_descr.FromImage = strings.Replace(clone_descr.FromImage, "$CSCS_REGISTRY_PATH", registry_path, 1)
    }
    if strings.HasPrefix(clone_descr.FromImage, "${CSCS_REGISTRY_PATH}") {
        clone_descr.FromImage = strings.Replace(clone_descr.FromImage, "${CSCS_REGISTRY_PATH}", registry_path, 1)
    }

    username := fmt.Sprintf("ci-ext-cloneimage-%v", xid.New().String())
    password, err := util.CreateJFrogUser(username, h.Config.JFrog.EMail, h.Config.JFrog.URL, h.Config.JFrog.Token)
    pie(logger, err, "Failed creating new username in JFrog", http.StatusInternalServerError)
    logger.Debug().Msgf("Successfully created new user in Jrog with name=%v", username)
    h.DB.AddCredential(0, job_id, username, password, util.SystemJFrogUser)

    registryAllowPaths, err := h.DB.GetRegistryAllowPaths(repository_id)
    pie(logger, err, "Failed getting allowed registry paths from database", http.StatusInternalServerError)
    permission_index := 0
    for repo, include_patterns := range registryAllowPaths {
        permissionName := fmt.Sprintf("%v-%v", username, permission_index)
        err = util.CreateJFrogPermission(permissionName, username, repo, include_patterns, h.Config.JFrog.URL, h.Config.JFrog.Token)
        pie(logger, err, "Failed creating permission", http.StatusInternalServerError)
        h.DB.AddCredential(0, job_id, permissionName, "", fmt.Sprintf("%v.%v", util.SystemJFrogPerm, permission_index))
        logger.Debug().Msgf("Successfully created new permission in jfrog with name=%v", permissionName)
        permission_index += 1
    }


    clone_descr.FromImage = fmt.Sprintf("docker://%v", clone_descr.FromImage)
    clone_descr.ToImage = fmt.Sprintf("docker://%v", clone_descr.ToImage)

    cmd := exec.Command("skopeo", "copy",
        "--retry-times", "5",
        "--src-creds", fmt.Sprintf("%v:%v", username, password),
        "--dest-creds", fmt.Sprintf("%v:%v", clone_descr.RegistryCreds.Username,clone_descr.RegistryCreds.Password),
        clone_descr.FromImage,
        clone_descr.ToImage)

    // connect Stdout/Stderr to the responseWriter, i.e. the caller will see the output (effectively we cannot modify the statuscode later on)
    cmd.Stdout = w
    cmd.Stderr = w
    err = cmd.Run()

    job_creds, err := h.DB.GetCredentials(int64(job_id))
    pie(logger, err, "Failed getting all job tokens from database", http.StatusInternalServerError)
    for _, cred := range job_creds {
        util.GetTokenDeleter().DeleteToken(cred)
    }

    pie(logger, err, "Copying image failed", http.StatusInternalServerError)
}
