package handler

import (
    "fmt"
    "net/http"
    "os"
    "runtime"
    "strconv"
    "strings"

    "github.com/xanzy/go-gitlab"
    wh_gitlab "github.com/go-playground/webhooks/v6/gitlab"
    "github.com/rs/zerolog"

    "cscs.ch/cicd-ext-mw/logging"
    "cscs.ch/cicd-ext-mw/util"
)

const request_log_html string = `The webhook event is being processed in the background.
Further details can be found at REPLACE_URL.
Please bear in mind that the background process can take several minutes and the log contents are incomplete for very recent events`

func GetWebhookGitlabPipelineHandler(glserver *gitlab.Client, db util.DB, config util.Config) func(w http.ResponseWriter, r *http.Request) {
    return wrap(webhookGitlabPipeline{glserver, db, config})
}

type webhookGitlabPipeline struct {
    glserver *gitlab.Client
    db util.DB
    config util.Config
}

func (h webhookGitlabPipeline) Post(w http.ResponseWriter, r *http.Request) {
    // run everything in background, because webhooks must be quick and successful. Any further debug information will be saved in a separate logfile that can be
    // retrieved later
    reqLogFile, err := os.OpenFile(h.config.GetRequestLogPath(logging.GetReqCorrelationID(r)), os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0600)
    pie(logging.GetReqLogger(r), err, "Failed opening requests log file for writing", http.StatusInternalServerError)
    logger, reqId := logging.LogReqHandlingToFile(r, reqLogFile)
    logger.Debug().Msg("Started request logging")
    go func() {
        defer func() {
            if panicVal := recover(); panicVal != nil {
                panicStr := fmt.Sprint(panicVal)
                stackbuf := make([]byte, 2048)
                n := runtime.Stack(stackbuf, false)
                stackbuf = stackbuf[:n]

                logger.Error().
                    Str("panic", panicStr).
                    Bytes("stack", stackbuf).
                    Str("id", reqId).
                    Msgf("%s %s failed with %v", r.Method, r.RequestURI, panicStr)
            }
            logger.Info().Msg("Finished processing webhook event")
            reqLogFile.Close()
        }()
    //    repo_id, err := strconv.ParseInt(r.URL.Query().Get("repository_id"), 10, 64)
        pipeline_id, err := strconv.ParseInt(r.URL.Query().Get("pipeline_id"), 10, 64)
        hook, err := wh_gitlab.New(wh_gitlab.Options.Secret(h.db.GetMirrorWebhookSecret(pipeline_id)))
        pie(logger, err, "Failed creating new webhook gitlab webook handler", http.StatusBadRequest)
        // there is some "bug" in the gitlab webhook, where a JobEvent is forwarded to a BuildEvent, therefore we need to catch the BuildEvent too
        // BuildEvent does not even exist in newer versions of gitlab. Also BuildEvent has fewer fields, but we do not need the additional
        // fields, so it is fine. I fixed the bug in my fork at github.com/finkandreas/webhooks, but we rely for consistency on the official
        // package. Also there is an open PR https://github.com/go-playground/webhooks/pull/146
        // Also I opened an issue about it: https://github.com/go-playground/webhooks/issues/175
        payload, err := hook.Parse(r, wh_gitlab.PipelineEvents, wh_gitlab.JobEvents, wh_gitlab.BuildEvents)
        if err != nil {
           if err != wh_gitlab.ErrEventNotFound {
               logger.Warn().Msgf("Received a gitlab webhook event that we did not expect. Header[X-Gitlab-Event]=%v", r.Header.Get("X-Gitlab-Event"))
           } else {
                pie(logger, err, "Failed parsing webhook payload", http.StatusBadRequest)
            }
        }
        switch payload.(type) {
            case wh_gitlab.PipelineEventPayload:
                h.handlePipelineEvent(payload.(wh_gitlab.PipelineEventPayload), pipeline_id, logger)
            case wh_gitlab.JobEventPayload:
                h.handleJobEvent(payload.(wh_gitlab.JobEventPayload), pipeline_id, logger)
            case wh_gitlab.BuildEventPayload:
                h.handleBuildEvent(payload.(wh_gitlab.BuildEventPayload), pipeline_id, logger)
        }
    }()
    w.Write([]byte(strings.Replace(request_log_html, "REPLACE_URL", fmt.Sprintf("%v/%v/webhook_log?reqId=%v", h.config.BaseURL, h.config.URLPrefix, reqId), 1)))
}

func (h webhookGitlabPipeline) handlePipelineEvent(e wh_gitlab.PipelineEventPayload, pipeline_id int64, logger *zerolog.Logger) {
    if h.db.GetMirrorUrl(pipeline_id) != e.Project.WebURL {
        pie(logger, condition_error{"The event URL does not match the registered mirror URL"}, "Mismatch of registered mirror URL and payload URL", http.StatusBadRequest)
    }
    if e.ObjectAttributes.Source != "parent_pipeline" && e.ObjectAttributes.Status != util.GlStatusSkipped {
        pipelinedata, _, err := h.glserver.Pipelines.GetPipeline(int(e.Project.ID), int(e.ObjectAttributes.ID))
        pie(logger, err, "Failed getting pipeline data via gitlab REST api", http.StatusInternalServerError)
        repo_id := h.db.GetRepositoryId(pipeline_id)
        commit_url := util.GetNotificationUrl(h.db.GetRepoUrl(repo_id), e.ObjectAttributes.SHA)
        status := e.ObjectAttributes.Status
        target_url := fmt.Sprintf("%v/%v/pipeline/results/%v/%v/%v?iid=%v", h.config.BaseURL, h.config.URLPrefix, pipeline_id, e.Project.ID, e.ObjectAttributes.ID, pipelinedata.IID) // e.ObjectAttributes.IID)
        if ! h.db.IsPrivateRepo(repo_id) {
            target_url = target_url + "&type=gitlab"
        }
        pipeline_name := h.db.GetPipelineName(pipeline_id)
        notification_token := h.db.GetNotificationToken(repo_id)

        switch status {
        case util.GlStatusSuccess, util.GlStatusFailed, util.GlStatusSkipped, util.GlStatusCanceled:
            h.db.DelStatusUpdateNeeded(pipeline_id, target_url, e.ObjectAttributes.SHA, pipeline_name)
        default:
            h.db.AddStatusUpdateNeeded(pipeline_id, target_url, e.ObjectAttributes.SHA, pipeline_name)
        }

        if notification_token == "" {
            logging.Infof("NotifyRepo at url=%v, but notification_token is empty. Not trying to notify", commit_url)
        } else {
            resp, err := util.NotifyRepo(commit_url, status, target_url, pipeline_name, notification_token, "")
            pie(logger, err, "Failed notifying original repo", http.StatusBadRequest)
            if err := util.CheckResponse(resp); err != nil {
                // most probably an incorrect notification token
                logger.Error().Err(err).Msgf("Failed notifiying original repository %v", h.db.GetRepositoryName(repo_id))
            }
        }
    }
}


func (h webhookGitlabPipeline) handleJobBuildEvent(url, status string, job_id, pipeline_id int64, logger *zerolog.Logger) {
    if !strings.HasPrefix(url, h.db.GetMirrorUrl(pipeline_id)) {
        pie(logger, condition_error{"The event URL does not match the registered mirror URL"}, "Mismatch of registered mirror URL and payload URL", http.StatusBadRequest)
    }
    if status == util.GlStatusSuccess || status == util.GlStatusFailed || status == util.GlStatusCanceled {
        job_creds, err := h.db.GetCredentials(job_id)
        pie(logger, err, "Failed getting all job tokens from database", http.StatusInternalServerError)
        for _, cred := range job_creds {
            util.GetTokenDeleter().DeleteToken(cred)
        }
    }
}
func (h webhookGitlabPipeline) handleBuildEvent(e wh_gitlab.BuildEventPayload, pipeline_id int64, logger *zerolog.Logger) {
    h.handleJobBuildEvent(e.Repository.GitHTTPURL, e.BuildStatus, e.BuildID, pipeline_id, logger)
}
func (h webhookGitlabPipeline) handleJobEvent(e wh_gitlab.JobEventPayload, pipeline_id int64, logger *zerolog.Logger) {
    h.handleJobBuildEvent(e.Repository.GitHTTPURL, e.BuildStatus, e.BuildID, pipeline_id, logger)
}
