package handler

import (
    "bytes"
    "fmt"
    "html/template"
    "net/http"
    "os"

    "cscs.ch/cicd-ext-mw/thirdparty/ansihtml"
    "cscs.ch/cicd-ext-mw/logging"
    "cscs.ch/cicd-ext-mw/util"
)

func GetWebhookLogHandler(config util.Config) func(w http.ResponseWriter, r *http.Request) {
    return wrap(webhookLogHandler{config: config})
}

type webhookLogHandler struct {
    config util.Config
}

func (h webhookLogHandler) Get(w http.ResponseWriter, r *http.Request) {
    logger := logging.GetReqLogger(r)
    reqId := r.URL.Query().Get("reqId")
    logFile, err := os.Open(h.config.GetRequestLogPath(reqId))
    pie(logger, err, "Could not find log file", http.StatusBadRequest)
    html_out := new(bytes.Buffer)
    ansihtml.ConvertToHTML(logFile, html_out)

    tmpl_data := webhookLogTmplData{
        Title: fmt.Sprintf("Log for request %v", reqId),
        ReqLog: template.HTML(html_out.String()),
        UrlPrefix: h.config.URLPrefix,
    }

    err = util.ExecuteTemplate(w, "webhook_log.html", &tmpl_data)
    pie(logger, err, "Error rendering template webhook_log.html", http.StatusInternalServerError)
}

type webhookLogTmplData struct {
    Title string
    ReqLog template.HTML
    UrlPrefix string
}
