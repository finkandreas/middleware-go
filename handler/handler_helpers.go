package handler

import (
    "net/http"
    "strconv"

    "github.com/rs/zerolog"

    "cscs.ch/cicd-ext-mw/logging"
    "cscs.ch/cicd-ext-mw/util"
)

type access_error struct {}
func (e access_error) Error() string {
    return "Access error"
}

type condition_error struct {
    Err string
}
func (e condition_error) Error() string {
    return "Some expected condition was not met. Err=" + e.Err
}

type db_access_error struct {
    Err string
}
func (e db_access_error) Error() string {
    return "There was an error accessing the database. Err=" + e.Err
}

type pipeline_not_found_error struct {
    PipelineName string
}
func (e pipeline_not_found_error) Error() string {
    return "Could not find pipeline with name=" + e.PipelineName
}


func pie(logger *zerolog.Logger, err error, msg string, statuscode int) {
    if err != nil {
        logger.Error().Err(err).Msg(msg)
        // logging/request_logger will recover from this panic, log it and write to the http.ResponseWriter
        panic(logging.NewHandlerError(err, msg, statuscode))
    }
}


// returns -1 if not authorized - no error is returned
func authorized_for_repo(r* http.Request) int64 {
    user, pass, ok := r.BasicAuth()
    // no BasicAuth header --> not authorized for any repo
    if ok == false {
        return -1
    }

    // username cannot be converted to a string --> not authorized to any repo
    repo_id, err := strconv.ParseInt(user, 10, 64)
    if err != nil {
        return -1
    }

    // repo_id and webhook_secret match --> authorized for this repo_id
    if util.GetDb().GetWebhookSecret(repo_id) == pass {
        return repo_id
    }

    // default is that we are not authorized to any repo
    return -1
}
